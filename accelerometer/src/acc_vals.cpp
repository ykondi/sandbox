#include "mbed.h"
#include "rtos.h"

 Serial pc(SERIAL_TX, SERIAL_RX);
 AnalogIn X(PA_0), Y(PA_1), Z(PB_0);
 DigitalIn mybutton(USER_BUTTON);
DigitalOut myled(LED1);
 
  float mapper(float val) {return (val - 0.35) ;}
 
  void AccelerometerControl(void const *args) {
    float x, y, z;
    int** arr = (int **) args;
    int* axes = *arr;
    while (1) {
       // Read data from 3 axes
       x = 1000*mapper(X.read());
       y = 1000*mapper(Y.read());
       z = 1000*mapper(Z.read());
       // Write to pc
       //pc.printf("%3.2f\t %3.2f\t %3.2f\n\r", x, y, z);
       axes[0] = x;
       axes[1] = y;
       axes[2] = z;
       wait(0.2);     
    }
}

 int main() {
     myled = 0;
     int *axes[3];
     axes[0] = (int *)malloc(sizeof(int) * 3);
     axes[1] = axes[0] + 1;
     axes[2] = axes[1] + 1;
     Thread AccelerometerThread (AccelerometerControl, (void *)axes);
     while(1) {
        if (!mybutton) {myled = !myled; wait(0.1); }
        if (myled) pc.printf("%d %d %d\n", *(axes[0]), *(axes[1]), *(axes[2]));
        else pc.printf("0 0 0\n");
    }
}
     
