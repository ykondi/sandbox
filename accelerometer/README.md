Accelerometer based input
=========================

As the name suggests. Small python script to read accelerometer values from
arg1, scale as per arg2 and arg3 and use it to control the cursor.

Usage: Connect accelerometer to microcontroller as per code, run:
    sudo modprobe uinput
    cat /dev/ttyACM0 | sudo python acc_mouse_control.py
